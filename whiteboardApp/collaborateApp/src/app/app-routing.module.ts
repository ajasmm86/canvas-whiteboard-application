

import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

const routes: Routes = [{
		path: 'auth',
		loadChildren: 'app/components/login/login.module#LoginModule'
	}, {
		path: 'home',
		loadChildren: 'app/components/home/home.module#HomeModule',
	}, {
		path: '',
		redirectTo: 'auth',
		pathMatch: 'full'
	}];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
