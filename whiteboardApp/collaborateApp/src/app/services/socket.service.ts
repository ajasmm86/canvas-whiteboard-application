import { Injectable } from '@angular/core';
import { Observable } from 'rxjs/Observable';

import * as io from 'socket.io-client';

import { environment } from './../../environments/environment';

/* importing interfaces starts */
import { Auth } from './../models/auth';
/* importing interfaces ends */

@Injectable()
export class SocketService {

	private BASE_URL = environment.socketUrl;
	private socket;

	constructor() { }

	/*
	* Method to connect the users to socket
	*/
	connectSocket(userId: string): void {
		this.socket = io(this.BASE_URL, { query: `userId=${userId}` });
	}

	/*
	* Method to emit the logout event.
	*/
	logout(userId: object): Observable<Auth> {
		this.socket.emit('logout', userId);
		return new Observable(observer => {
			this.socket.on('logout-response', (data: Auth) => {
				observer.next(data);
			});
			return () => {
				this.socket.disconnect();
			};
		});
	}

	/*
	* Method to receive chat-list-response event.
	*/
	getChatList(userId: string = null): Observable<any> {
		if (userId !== null) {
			this.socket.emit('chat-list', { userId: userId });
		}
		return new Observable(observer => {
			this.socket.on('chat-list-response', (data: any) => {
				observer.next(data);
			});
			return () => {
				this.socket.disconnect();
			};
		});
	}

	/*
	* Method to emit the add-messages event.
	*/
	sendMessage(message): void {
		 this.socket.emit('add-message', message);
	}

	/*
	* Method to receive add-message-response event.
	*/
	receiveMessages(): Observable<any> {
		return new Observable(observer => {
			this.socket.on('add-message-response', (data) => {
				observer.next(data);
			});

			return () => {
				this.socket.disconnect();
			};
		});
	}

}
