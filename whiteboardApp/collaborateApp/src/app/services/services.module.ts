

import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

/**
 * Importing services starts
 */

import { FormService } from './form.service';
import { ChatService } from './chat.service';
/* Todo - remove import { AuthGuardService } from './auth-guard.service';*/
import { SocketService } from './socket.service';
import { EmitterService } from './emitter.service';
/**
 * Importing services ends
 */

@NgModule({
	imports: [
		CommonModule
	],
	declarations: [],
	providers: [ /* Adding services into providers array*/
		ChatService,
		SocketService,
		EmitterService,
		FormService
	]
})
export class ServicesModule { }
