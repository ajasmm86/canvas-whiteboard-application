import { Injectable } from '@angular/core';
import { FormBuilder, FormGroup } from '@angular/forms';

@Injectable()
export class FormService {
	constructor() {}

	createLoginForm(): FormGroup {
		return new FormBuilder().group({
			username: '',
			password: '',
		});
	}

	createRegistrationForm(): FormGroup {
		return new FormBuilder().group({
			username: '',
			password: '',
		});
	}

	createMessageForm(): FormGroup {
		return new FormBuilder().group({
			message: ''
		});
	}
}
