import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Router } from '@angular/router';

import { Observable } from 'rxjs/Observable';
import { catchError } from 'rxjs/operators';
import { ErrorObservable } from 'rxjs/observable/ErrorObservable';
import 'rxjs/add/operator/map';

import { environment } from './../../environments/environment';

/* importing interfaces starts */

import { AuthRequest } from './../models/auth-request';
import { Auth } from './../models/auth';
import { MessagesResponse } from './../models/messages-response';
/* importing interfaces ends */

@Injectable()
export class ChatService {

	private BASE_URL = environment.apiUrl;
	private httpOptions = {
		headers: new HttpHeaders({
			'Content-Type': 'application/json',
			'Authorization': 'my-auth-token'
		})
	};

	constructor(
		private http: HttpClient,
		public router: Router
	) { }

	getUserId(): Promise<string> {
		return new Promise( (resolve, reject) => {
			try {
				resolve(localStorage.getItem('userid'));
			} catch (error) {
				reject(error);
			}
		});
	}

	removeLS(): Promise<boolean> {
		return new Promise((resolve, reject) => {
			try {
				localStorage.removeItem('userid');
				localStorage.removeItem('username');
				resolve(true);
			} catch (error) {
				reject(error);
			}
		});
	}


	login(params: AuthRequest): Observable<Auth> {
		return this.http.post(`${this.BASE_URL}login`, JSON.stringify(params), this.httpOptions).map(
			(response: Auth) => {
				return response;
			},
			(error) => {
				throw error;
			}
		);
	}

	register(params: AuthRequest): Observable<Auth> {
		return this.http.post(`${this.BASE_URL}register`, JSON.stringify(params), this.httpOptions).map(
			(response: Auth) => {
				return response;
			},
			(error) => {
				throw error;
			}
		);
	}

	getMessages(params: any): Observable<MessagesResponse> {
		return this.http.post(`${this.BASE_URL}getMessages`, JSON.stringify(params), this.httpOptions).pipe(
			catchError(error => {
				alert('Something bad happened; please try again later.');
				return new ErrorObservable(
					'Something bad happened; please try again later.');
			})
		);
	}
}
