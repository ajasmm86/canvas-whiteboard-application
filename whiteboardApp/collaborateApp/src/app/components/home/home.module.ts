import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { NgbModule } from '@ng-bootstrap/ng-bootstrap';

import { ChatListModule } from './../chat-list/chat-list.module';
import { HomeRoutingModule } from './home-routing.module';
import { HomeComponent } from './home.component';
import { CollabCanvasDirective } from '../../directive/collab-canvas.directive';

@NgModule({
	imports: [
		CommonModule,
		HomeRoutingModule,
		NgbModule.forRoot(),
		ChatListModule
		
	],
	declarations: [HomeComponent,CollabCanvasDirective]
})
export class HomeModule { }
