import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { FormGroup } from '@angular/forms';

/* Importing services starts*/
import { ChatService } from './../../services/chat.service';
import { FormService } from './../../services/form.service';
/* Importing services ends*/

/* Importing Models starts*/
import { Auth } from './../../models/auth';
/* Importing inrerface starts*/

@Component({
	selector: 'app-auth',
	templateUrl: './login.component.html',
	styleUrls: ['./login.component.css']
})

export class LoginComponent implements OnInit {

	public setTabPosition = 'center';
	public overlayDisplay = true;
	

	private loginForm: FormGroup;
	private registrationForm: FormGroup;

	constructor(
		private router: Router,
		private chatService: ChatService,
		private formService: FormService
	) {
		this.loginForm = this.formService.createLoginForm();
		this.registrationForm = this.formService.createRegistrationForm();
	}

	ngOnInit() {
	}

	login(): void {
		if (this.loginForm.valid) {
			this.overlayDisplay = false;
			this.chatService.login(this.loginForm.value).subscribe(
				(response: Auth) => {
					localStorage.setItem('userid', response.userId);
					this.router.navigate(['/home']);
				},
				(error) => {
					this.overlayDisplay = true;
					alert('Inavalid Login Credentials, try again.');
				}
			);
		}
	}

	register(): void {
		if (this.registrationForm.valid) {
			this.overlayDisplay = false;
			this.chatService.register(this.registrationForm.value).subscribe(
				(response: Auth) => {
					this.overlayDisplay = true;
					localStorage.setItem('userid', response.userId);
					this.router.navigate(['/home']);
				},
				(error) => {
					this.overlayDisplay = true;
					alert('Something bad happened; please try again later.');
				}
			);
		}
	}

}
