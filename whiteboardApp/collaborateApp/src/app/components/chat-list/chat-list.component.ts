import { Component, OnInit, Input } from '@angular/core';
import { Router } from '@angular/router';

/* Importing services starts*/
import { ChatService } from './../../services/chat.service';
import { SocketService } from './../../services/socket.service';
import { EmitterService } from './../../services/emitter.service';
/* Importing services ends*/

/* importing interfaces starts */
import { MessagesResponse } from './../../models/messages-response';
/* importing interfaces ends */

@Component({
  selector: 'app-chat-list',
  templateUrl: './chat-list.component.html',
  styleUrls: ['./chat-list.component.css']
})
export class ChatListComponent {

	private userId: string = null;
	public chatListUsers: any[] = [];
	private selectedUserId: string = null;

	/* Incoming data from other component starts */
	@Input() conversation: string;
	@Input() selectedUserInfo: string;
	/* Incoming data from other component ends */

	constructor(
		private chatService: ChatService,
		private socketService: SocketService,
		private router: Router
	) { }

	getChatList(socketIOResponse: any, userId: string): void {
		this.userId = userId;
		if (!socketIOResponse.error) {
			if (socketIOResponse.singleUser) {
				/* Adding new online user into chat list array */
				this.chatListUsers = this.chatListUsers.concat(socketIOResponse.chatList);
			} else if (socketIOResponse.userDisconnected) {
				const loggedOutUser = this.chatListUsers.findIndex((obj: any) => obj.id === socketIOResponse.userid );
				if (loggedOutUser >= 0) {
					this.chatListUsers[loggedOutUser].online = 'N';
				}
			} else {
				/* Updating entire chatlist if user logs in. */
				this.chatListUsers = socketIOResponse.chatList;
			}
		} else {
			alert(`Unable to load Chat list, Redirecting to Login.`);
			this.chatService.removeLS()
			.then( (removedLs: boolean) => {
				this.router.navigate(['/']);
			})
			.catch((error: Error) => {
				alert(' This App is Broken, we are working on it. try after some time.');
				throw error;
			});
		}
	}


	isUserSelected(userId: string): boolean {
		if (!this.selectedUserId) {
			return false;
		}
		return this.selectedUserId === userId ? true : false;
	}


}
