import {
  Directive,
  ElementRef,
  HostListener,
  HostBinding,
  AfterViewInit,
} from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { SocketService } from '../services/socket.service';

declare interface Position {
  offsetX: number;
  offsetY: number;
}
@Directive({
  selector: '[appCollabCanvas]'
})
export class CollabCanvasDirective implements AfterViewInit {
  canvas: HTMLCanvasElement;
  ctx: CanvasRenderingContext2D;
  // Stroke styles for user and guest
  userStrokeStyle = '#FAD8D6';
  guestStrokeStyle = '#CD5334';
  position: {
    start: {};
    stop: {};
  };
  // This will hold a list of positions recorded throughout the duration of a paint event
  line = [];
  
  // This object will hold the start point of any paint event.
   prevPos: Position = {
    offsetX: 0,
    offsetY: 0,
  };
  // This will be set to true when a user starts painting
  isPainting = false;
  constructor(private el: ElementRef,
    private http: HttpClient, private socketService: SocketService) {
    this.canvas = this.el.nativeElement;
    this.canvas.width = 1000;
    this.canvas.height = 800;
    // We create a canvas context. 
    this.ctx = this.canvas.getContext('2d');
    this.ctx.lineJoin = 'round';
    this.ctx.lineCap = 'round';
    this.ctx.lineWidth = 5;
  }

  @HostListener('mousedown', ['$event'])
  onMouseDown({ offsetX, offsetY }) {
    this.isPainting = true;
    // Get the offsetX and offsetY properties of the event. 
    this.prevPos = {
      offsetX,
      offsetY,
    };
  }
  @HostListener('mousemove', ['$event'])
  onMouseMove({ offsetX, offsetY }) {
    if (this.isPainting) {
      const offSetData = { offsetX, offsetY };
      // Set the start and stop position of the paint event. 
      this.position = {
        start: { ...this.prevPos },
        stop: { ...offSetData },
      };
      // Add the position to the line array
      this.line = this.line.concat(this.position);
      this.draw(this.prevPos, offSetData, this.userStrokeStyle);
    }
  }
  @HostListener('mouseup')
  onMouseUp() {
    if (this.isPainting) {
      this.isPainting = false;
      // Send a request to the server at the end of a paint event
      this.makeRequest();
    }
  }
  @HostListener('mouseleave')
  onmouseleave() {
    if (this.isPainting) {
      this.isPainting = false;
      this.makeRequest();
    }
  }
  @HostBinding('style.background') background = 'black';

  makeRequest() {
    // Make a request to the server containing the user's Id and the line array.
    this.socketService.sendMessage({
      fromUserId: localStorage.getItem('userid'),
      message: this.line,
      toUserId: ''
    });

  }
  // The draw method takes three parameters; the prevPosition, currentPosition and the strokeStyle
  draw(
    { offsetX: x, offsetY: y }: Position,
    { offsetX, offsetY }: Position,
    strokeStyle
  ){
    // begin drawing
    this.ctx.beginPath();
    this.ctx.strokeStyle = strokeStyle;
    // Move the the prevPosition of the mouse
    this.ctx.moveTo(x, y);
    // Draw a line to the current position of the mouse
    this.ctx.lineTo(offsetX, offsetY);
    // Visualize the line using the strokeStyle
    this.ctx.stroke();
    this.prevPos = {
      offsetX,
      offsetY,
    };
  }
  ngAfterViewInit() {

    /* Calling Compoenent method to Listen for Incoming Messages*/
			this.socketService.receiveMessages().subscribe((data) => {
          console.log(JSON.stringify(data));
          data['message'].forEach((position) => {
            this.draw(position.start, position.stop, this.guestStrokeStyle);
          });
			});
  }


}
