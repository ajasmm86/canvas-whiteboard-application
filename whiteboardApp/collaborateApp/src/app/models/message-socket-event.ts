export interface MessageSocketEvent {
	fromUserId: string;
	message: [''];
	toUserId: string;
}
